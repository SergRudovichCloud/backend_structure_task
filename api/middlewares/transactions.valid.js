const joi = require('joi');
const isValid = require('../../helpers/is.valid');

const isTransactionValid = (req, res, next) => {
    var schema = joi.object({
        id: joi.string().uuid(),
        userId: joi.string().uuid().required(),
        cardNumber: joi.string().required(),
        amount: joi.number().min(0).required(),
    }).required();
    isValid(schema, req.body, res);
    next();
}

module.exports = {
    isTransactionValid
}