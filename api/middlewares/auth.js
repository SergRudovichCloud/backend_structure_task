
const jwt = require("jsonwebtoken");

const isAuthorized = (req, res, next) => {
    let token = req.headers['authorization'];
    let tokenPayload;
    if (!token) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
    token = token.replace('Bearer ', '');
    try {
        tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        if (req.params.id !== tokenPayload.id) {
            return res.status(401).send({ error: 'UserId mismatch' });
        }
    } catch (err) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
    next();
}


module.exports = isAuthorized;