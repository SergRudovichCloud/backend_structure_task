const joi = require('joi');
const isValid = require('../../helpers/is.valid');

const isBetValid = (req, res, next) => {
    var schema = joi.object({
        id: joi.string().uuid(),
        eventId: joi.string().uuid().required(),
        betAmount: joi.number().min(1).required(),
        prediction: joi.string().valid('w1', 'w2', 'x').required(),
    }).required();
    isValid(schema, req.body, res);
    next();
}

module.exports = {
    isBetValid
}