const joi = require('joi');
const isValid = require('../../helpers/is.valid');

const phoneRegex = /^\+?3?8?(0\d{9})$/;

const isUserIdValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
    }).required();
    isValid(schema, req.params, res);
    next();
}

const isUserPostValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        type: joi.string().required(),
        email: joi.string().email().required(),
        phone: joi.string().pattern(phoneRegex).required(),
        name: joi.string().required(),
        city: joi.string(),
    }).required();
    isValid(schema, req.body, res);
    next();
}

const isUserPutValid = (req, res, next) => {
    const schema = joi.object({
        email: joi.string().email(),
        phone: joi.string().pattern(phoneRegex),
        name: joi.string(),
        city: joi.string(),
    }).required();
    const isValidResult = schema.validate(req.body);
    if (isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
        return;
    };
    next();
}

module.exports = {
    isUserIdValid,
    isUserPostValid,
    isUserPutValid
}