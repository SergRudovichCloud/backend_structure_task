const joi = require('joi');
const isValid = require('../../helpers/is.valid');

const isEventIdValid = (req, res, next) => {
    const schema = joi.object({
        score: joi.string().required(),
    }).required();
    isValid(schema, req.body, res);
    next();
}

const isEventValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        type: joi.string().required(),
        homeTeam: joi.string().required(),
        awayTeam: joi.string().required(),
        startAt: joi.date().required(),
        odds: joi.object({
            homeWin: joi.number().min(1.01).required(),
            awayWin: joi.number().min(1.01).required(),
            draw: joi.number().min(1.01).required(),
        }).required(),
    }).required();
    var isValidResult = schema.validate(req.body);
    if (isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
        return;
    };
    next();
}

module.exports = {
    isEventIdValid,
    isEventValid
}