const isAdmin = require('../middlewares/auth.admin');
const { isTransactionValid } = require('../middlewares/transactions.valid');
const {
    transactionRepository,
    userRepository,
} = require('../../db/repository/base.repository');
const express = require('express');
const router = express.Router();

router
    .post("/", isTransactionValid, isAdmin, (req, res) => {
        userRepository.getById(req.body.userId)
            .then(([user]) => {
                if (!user) {
                    res.status(400).send({ error: 'User does not exist' });
                    return;
                }
                req.body.card_number = req.body.cardNumber;
                delete req.body.cardNumber;
                req.body.user_id = req.body.userId;
                delete req.body.userId;
                transactionRepository.add(req.body)
                    .then(([result]) => {
                        var currentBalance = req.body.amount + user.balance;
                        userRepository.update(req.body.user_id, { balance: currentBalance })
                            .then(() => {
                                ['user_id', 'card_number', 'created_at', 'updated_at'].forEach(whatakey => {
                                    var index = whatakey.indexOf('_');
                                    var newKey = whatakey.replace('_', '');
                                    newKey = newKey.split('')
                                    newKey[index] = newKey[index].toUpperCase();
                                    newKey = newKey.join('');
                                    result[newKey] = result[whatakey];
                                    delete result[whatakey];
                                })
                                return res.send({
                                    ...result,
                                    currentBalance,
                                });
                            });
                    });
            }).catch(err => {
                res.status(500).send("Internal Server Error");
                return;
            });
    });

module.exports = router;