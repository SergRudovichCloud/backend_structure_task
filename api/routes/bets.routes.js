const { isBetValid } = require('../middlewares/bet.valid');
const jwt = require("jsonwebtoken");
const statEmitter = require('../../helpers/emitter');
const handleErr = require('../../helpers/handle.err');
const {
    oddRepository,
    userRepository,
    eventRepository,
    betRepository
} = require('../../db/repository/base.repository');

const express = require('express');
const router = express.Router();

router
    .post("/", isBetValid, (req, res) => {
        let userId;
        let token = req.headers['authorization'];
        if (!token) {
            return res.status(401).send({ error: 'Not Authorized' });
        }
        token = token.replace('Bearer ', '');
        try {
            var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
            userId = tokenPayload.id;
        } catch (err) {
            console.log(err);
            return res.status(401).send({ error: 'Not Authorized' });
        }
        req.body.event_id = req.body.eventId;
        req.body.bet_amount = req.body.betAmount;
        delete req.body.eventId;
        delete req.body.betAmount;
        req.body.user_id = userId;
        userRepository.getById(userId).then(([user]) => {
            if (!user) {
                res.status(400).send({ error: 'User does not exist' });
                return;
            }
            if (+user.balance < +req.body.bet_amount) {
                return res.status(400).send({ error: 'Not enough balance' });
            }
            eventRepository.getById(req.body.event_id)
                .then(([event]) => {
                    if (!event) {
                        return res.status(404).send({ error: 'Event not found' });
                    }
                    oddRepository.getById(event.odds_id)
                        .then(([odds]) => {
                            if (!odds) {
                                return res.status(404).send({ error: 'Odds not found' });
                            }
                            let multiplier;
                            switch (req.body.prediction) {
                                case 'w1':
                                    multiplier = odds.home_win;
                                    break;
                                case 'w2':
                                    multiplier = odds.away_win;
                                    break;
                                case 'x':
                                    multiplier = odds.draw;
                                    break;
                            }
                            betRepository.add({
                                ...req.body,
                                multiplier,
                                event_id: event.id
                            }).then(([bet]) => {
                                var currentBalance = user.balance - req.body.bet_amount;
                                userRepository.update(userId, {
                                    balance: currentBalance,
                                })
                                    .then(() => {
                                        statEmitter.emit('newBet');
                                        ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at', 'user_id'].forEach(whatakey => {
                                            var index = whatakey.indexOf('_');
                                            var newKey = whatakey.replace('_', '');
                                            newKey = newKey.split('')
                                            newKey[index] = newKey[index].toUpperCase();
                                            newKey = newKey.join('');
                                            bet[newKey] = bet[whatakey];
                                            delete bet[whatakey];
                                        });
                                        return res.send({
                                            ...bet,
                                            currentBalance: currentBalance,
                                        });
                                    });
                            });
                        });
                });
        }).catch(err => {
            handleErr(err, res)
        });
    });

module.exports = router;