const betsRouters = require('./bets.routes');
const eventsRouters = require('./events.routes');
const statsRouters = require('./stats.routes');
const usersRouters = require('./users.routes');
const transactionsRouters = require('./transactions.routes');

module.exports = (app) => {
    app.use("/bets", betsRouters);
    app.use("/events", eventsRouters);
    app.use("/stats", statsRouters);
    app.use("/users", usersRouters);
    app.use("/transactions", transactionsRouters);
}
