const jwt = require("jsonwebtoken");
const statEmitter = require('../../helpers/emitter');
const {
    isUserIdValid,
    isUserPostValid,
    isUserPutValid
} = require('../middlewares/user.valid');
const isAuthorized = require('../middlewares/auth');
const handleErr = require('../../helpers/handle.err');
const { userRepository } = require('../../db/repository/base.repository');

const express = require('express');
const router = express.Router();

router.get("/:id", isUserIdValid, (req, res) => {
    userRepository.getById(req.params.id)
        .then(([result]) => {
            if (!result) {
                res.status(404).send({ error: 'User not found' });
                return;
            }
            return res.send({
                ...result,
            });
        }).catch(err => { handleErr(err, res) });
});

router.post("/", isUserPostValid, (req, res) => {
    req.body.balance = 0;
    userRepository.add(req.body)
        .then(([result]) => {
            result.createdAt = result.created_at;
            delete result.created_at;
            result.updatedAt = result.updated_at;
            delete result.updated_at;
            statEmitter.emit('newUser');
            return res.send({
                ...result,
                accessToken: jwt.sign({ id: result.id, type: result.type }, process.env.JWT_SECRET)
            });
        }).catch(err => { handleErr(err, res) });
});

router.put("/:id", isAuthorized, isUserPutValid, (req, res) => {
    userRepository.update(req.params.id, req.body)
        .then(([result]) => {
            return res.send({
                ...result,
            });
        }).catch(err => { handleErr(err, res) });
});

module.exports = router;