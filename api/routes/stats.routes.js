const stats = require('../../config/stats');
const isAdmin = require('../middlewares/auth.admin');

const express = require('express');
const router = express.Router();

router
    .get("/", isAdmin, (req, res) => {
        res.send(stats);
    });

module.exports = router;