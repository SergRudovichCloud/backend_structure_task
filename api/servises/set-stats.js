var stats = require('../../config/stats');
var statEmitter = require('../../helpers/emitter');

module.exports = ()=>{
    statEmitter.on('newUser', () => {
        stats.totalUsers++;
      });
      statEmitter.on('newBet', () => {
        stats.totalBets++;
      });
      statEmitter.on('newEvent', () => {
        stats.totalEvents++;
      });
}