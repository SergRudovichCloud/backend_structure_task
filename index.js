var express = require("express");
var routes = require('./api/routes/index');
var db = require('./db/connection');
var setStats = require('./api/servises/set-stats');

var app = express();

var port = 3000;

app.use(express.json());

db.raw('select 1+1 as result').then(() => {
  console.log('Db connect success')
}).catch(() => {
  throw new Error('No db connection');
});

routes(app);

app.get("/health", (req, res) => {
  res.send("Hello World!");
});

app.listen(port, () => {
  setStats();
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };