const sendErr = (err) => {
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
}

module.exports = sendErr;