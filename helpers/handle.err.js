const sendErr = require('./sendErr');

const handleErr = (err, res) => {
    if (err.code == '23505') {
        console.log(err);
        res.status(400).send({
            error: err.detail
        });
        return;
    }
    sendErr(err);
}

module.exports = handleErr;