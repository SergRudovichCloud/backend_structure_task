const isValid = (schema, param, res) => {
    const isValidResult = schema.validate(param);
    if (isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
        return;
    };
}

module.exports = isValid;