var dbConfig = require("../knexfile");
var knex = require("knex");

module.exports = knex(dbConfig.development);
