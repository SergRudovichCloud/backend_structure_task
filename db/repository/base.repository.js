const db = require('../connection');

class BaseRepository {
    constructor(model){
        this.model = model;
    }
    getById(id) {
        return db(this.model).where('id', id).returning("*");
    }
    add(payload) {
        return db(this.model).insert(payload).returning("*");
    }
    update(id, payload) {
        return db(this.model).where('id', id).update(payload).returning("*");
    }
}

module.exports = {
    eventRepository: new BaseRepository('event'),
    oddRepository: new BaseRepository('odds'),
    userRepository: new BaseRepository("user"),
    betRepository: new BaseRepository("bet"),
    transactionRepository: new BaseRepository("transaction")
}